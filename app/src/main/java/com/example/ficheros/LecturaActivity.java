package com.example.ficheros;

import android.Manifest;
import android.content.pm.PackageManager;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class LecturaActivity extends AppCompatActivity {
    public final static String RAW = "numero";//No es numero.txt
    public final static String ASSETS = "valor.txt";
    public final static String INTERNA = "dato.txt";
    public final static String EXTERNA = "dato_sd.txt";
    public final static String RESULTADO = "operaciones.txt";
    public final static String CODIFICACION = "UTF-8";
    public final static int REQUEST_WRITE_EXTERNA = 1;
    public final static int REQUEST_WRITE_RESULTADO = 2;

    private Memoria memoria;
    private Resultado resultado;

    @BindView(R.id.edRaw)
    EditText edRaw;

    @BindView(R.id.edAssets)
    EditText edAssets;

    @BindView(R.id.edInterna)
    EditText edInterna;

    @BindView(R.id.edExterna)
    EditText edExterna;

    @BindView(R.id.tvResultado)
    TextView tvResultado;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lectura);

        ButterKnife.bind(this);
        iniciar();
    }

    private void iniciar() {
        memoria = new Memoria(this);
        leerRaw();
        leerAsset();
        leerInterna();
        escribirLeerExterna(EXTERNA, "7", false, CODIFICACION);
    }

    private void leerRaw() {
        resultado = memoria.leerRaw(RAW);
        if (resultado.getCodigo()){
            edRaw.setText(resultado.getContenido());
        } else {
            edRaw.setText("0");
            Toast.makeText(this, "Error al leer de la carpeta 'raw'", Toast.LENGTH_SHORT).show();
        }
    }

    private void leerAsset() {
        resultado = memoria.leerAsset(ASSETS);
        if (resultado.getCodigo()){
            edAssets.setText(resultado.getContenido());
        } else {
            edAssets.setText("0");
            Toast.makeText(this, "Error al leer de la carpeta 'assets'", Toast.LENGTH_SHORT).show();
        }
    }

    private void leerInterna() {
        resultado = memoria.leerInterna(INTERNA, CODIFICACION);
        if (resultado.getCodigo()){
            edInterna.setText(resultado.getContenido());
        } else {
            edInterna.setText("0");
            Toast.makeText(this, "Error al leer de la memoria interna", Toast.LENGTH_SHORT).show();
        }
    }

    private void extraerValorExterna() {
        resultado = memoria.leerExterna(EXTERNA, CODIFICACION);
        if (resultado.getCodigo()){
            edExterna.setText(resultado.getContenido());
        } else {
            edExterna.setText("0");
            Toast.makeText(this, "Error al leer de la memoria externa", Toast.LENGTH_SHORT).show();
        }
    }

    /*
    Pedimos permisos porque este ejercicio es muy cutre y escribe el fichero antes de leerlo,
    vaya locura, pero es lo que ha hecho @paco.portada. Lo que yo haría es leer el valor que ya
    hemos guardado en EscribirExternaActivity.
    Además de pedir los permisos escribe el fichero. Luego lo leemos en el método
    onRequestPermissionsResult([...]) en caso de que los permisos sean concedidos.
     */
    private void escribirLeerExterna(String fichero, String contenido, boolean b, String codificacion) {
        // comprobar los permisos
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            // pedir los permisos necesarios, porque no están concedidos
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, REQUEST_WRITE_EXTERNA);
            // Cuando se cierre el cuadro de diálogo se ejecutará onRequestPermissionsResult
        } else {
            // Permisos ya concedidos
            escribir(fichero, contenido, b, codificacion);
            extraerValorExterna();
        }
    }

    private void escribir(String fichero, String s, boolean b, String codificacion) {
        String mensaje;
        if (memoria.disponibleEscritura()){
            if (memoria.escribirExterna(fichero, s, b, codificacion)){
                mensaje = "Fichero escrito con éxito";
            } else {
                mensaje = "Error al escribir en el fichero";
            }
        } else {
            mensaje = "Memoria externa no disponible";
        }
        Toast.makeText(this, mensaje, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults){
// chequeo los permisos de nuevo
        switch (requestCode) {
            case REQUEST_WRITE_EXTERNA:
                if (ActivityCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
                    // permiso concedido
                    escribir(EXTERNA, "7", false, CODIFICACION);
                    extraerValorExterna();
                } else {
                    // no hay permiso
                    Toast.makeText(this, "No hay permiso para escribir en la memoria externa", Toast.LENGTH_SHORT).show();
                }
                break;
            case REQUEST_WRITE_RESULTADO:
                if (ActivityCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
                    // permiso concedido
                    escribir(RESULTADO, tvResultado.getText().toString(), false, CODIFICACION);
                } else {
                    // no hay permiso
                    Toast.makeText(this, "No hay permiso para escribir en la memoria externa", Toast.LENGTH_SHORT).show();
                }
                break;
        }
    }

    @OnClick(R.id.btSumar)
    public void sumar(){
        int cantidad;
        String operacion, mensaje;

        try {
            cantidad = Integer.valueOf(edRaw.getText().toString()) + Integer.valueOf(edAssets.getText().toString()) + Integer.valueOf(edInterna.getText().toString()) + Integer.valueOf(edExterna.getText().toString());
        } catch (NumberFormatException e) {
            e.printStackTrace();
            Toast.makeText(this, "Error en los valores", Toast.LENGTH_SHORT).show();
        }
    }
}
